import {Component, OnInit, ViewChild} from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {ProductDetailComponent} from './product.detail.component';
import {ProductFormComponent} from './product-form.component';

@Component({
    selector: 'my-app',
    template: `<h1>Intro to Angular 2 forms</h1>
            <div>
                <product-form [(productList)] = "_products"></product-form>
            </div>
            <hr>
            <ul>
                <li *ngFor="#product of _products">
                    <product-detail [product]="product"></product-detail>
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [ProductDetailComponent, ProductFormComponent]
})
export class AppComponent implements OnInit {
     private _products: ProductType[];
     private _boughtProducts: ProductType[] = [];

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }
}