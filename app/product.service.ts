import {Injectable} from 'angular2/core';
import {ProductType} from './product';


@Injectable()
export class ProductService {
    public getProducts() : ProductType[] {
        return [];
        /**
        return [new ProductType('ATI Radeon', 5000, 'http://pctuning.tyden.cz/ilustrace3/Sulc/radeon_hd5870/hd4870.jpg'),
                new ProductType('NVidia GTX 750', 6000, 'http://www.gigabyte.com.au/News/1272/2.jpg')];
                */
    }
}