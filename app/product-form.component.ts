import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {ProductType} from './product';
import {FORM_DIRECTIVES} from 'angular2/common';

@Component ({
    selector: 'product-form',
    template: `
           <h1>Enter new product details</h1>
           <form *ngIf="active" #f="ngForm" (ngSubmit) = 'onSubmit(f.value)'>
                <div>Enter name: <input type="text" ngControl="name"/></div>
                <div>Enter price: <input type="text" ngControl="price"/></div>
                <div>Enter image: <input type="text" ngControl="image"/></div> 
                <div><button type="submit">CREATE</button></div>
           </form> 
    `,
    directives: [FORM_DIRECTIVES]
})
export class ProductFormComponent {
    @Input() productList: ProductType[];
    @Output() productListChange = new EventEmitter<ProductType[]>();

    active: boolean = true;

    onSubmit(submitedValue: ProductType) {
        this.productList.push(submitedValue);
        this.productListChange.emit(this.productList);
        alert('Product has been added!');
        this.resetForm();
    }

    resetForm() : void {
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }
}