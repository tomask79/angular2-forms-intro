System.register(['angular2/core', 'angular2/common'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1;
    var ProductFormComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            }],
        execute: function() {
            ProductFormComponent = (function () {
                function ProductFormComponent() {
                    this.productListChange = new core_1.EventEmitter();
                    this.active = true;
                }
                ProductFormComponent.prototype.onSubmit = function (submitedValue) {
                    this.productList.push(submitedValue);
                    this.productListChange.emit(this.productList);
                    alert('Product has been added!');
                    this.resetForm();
                };
                ProductFormComponent.prototype.resetForm = function () {
                    var _this = this;
                    this.active = false;
                    setTimeout(function () { return _this.active = true; }, 0);
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], ProductFormComponent.prototype, "productList", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], ProductFormComponent.prototype, "productListChange", void 0);
                ProductFormComponent = __decorate([
                    core_1.Component({
                        selector: 'product-form',
                        template: "\n           <h1>Enter new product details</h1>\n           <form *ngIf=\"active\" #f=\"ngForm\" (ngSubmit) = 'onSubmit(f.value)'>\n                <div>Enter name: <input type=\"text\" ngControl=\"name\"/></div>\n                <div>Enter price: <input type=\"text\" ngControl=\"price\"/></div>\n                <div>Enter image: <input type=\"text\" ngControl=\"image\"/></div> \n                <div><button type=\"submit\">CREATE</button></div>\n           </form> \n    ",
                        directives: [common_1.FORM_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [])
                ], ProductFormComponent);
                return ProductFormComponent;
            }());
            exports_1("ProductFormComponent", ProductFormComponent);
        }
    }
});
//# sourceMappingURL=product-form.component.js.map