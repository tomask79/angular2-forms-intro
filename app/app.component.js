System.register(['angular2/core', './product.service', './product.detail.component', './product-form.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, product_service_1, product_detail_component_1, product_form_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (product_service_1_1) {
                product_service_1 = product_service_1_1;
            },
            function (product_detail_component_1_1) {
                product_detail_component_1 = product_detail_component_1_1;
            },
            function (product_form_component_1_1) {
                product_form_component_1 = product_form_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(productService) {
                    this.productService = productService;
                    this._boughtProducts = [];
                }
                AppComponent.prototype.ngOnInit = function () {
                    this._products = this.productService.getProducts();
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "<h1>Intro to Angular 2 forms</h1>\n            <div>\n                <product-form [(productList)] = \"_products\"></product-form>\n            </div>\n            <hr>\n            <ul>\n                <li *ngFor=\"#product of _products\">\n                    <product-detail [product]=\"product\"></product-detail>\n                </li>\n            </ul>           \n    ",
                        providers: [product_service_1.ProductService],
                        directives: [product_detail_component_1.ProductDetailComponent, product_form_component_1.ProductFormComponent]
                    }), 
                    __metadata('design:paramtypes', [product_service_1.ProductService])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map