# Writing applications in Angular 2, [part 7] #

## Introduction to Forms ##

If it's something where *Angular JS 1* really shines then it is amazing concept of Forms. Luckily a lot of these concepts stayed in Angular 2 as well.  
So let's take a look at them, first let's shed some light at basic architecture. Forms in Angular 2 runs around two fundamental objects.

* **Control** - wrapper around form component's value and states (validity, errors and dirty state). It's a smallest unit of Angular 2 form.
* **ControlGroup** - wrapper around list of Controls in a form. It contains state of our whole form, collected from the Controls contained in there. 
                 Yes, with ControlGroup you can check the state of the whole form as with the one Control.


## Building form in Angular 2, steps ##

To start to work with Controls and ControlGroups in Angular 2, you need to first import them into your component:

```
.
.
import {FORM_DIRECTIVES} from 'angular2/common';

@Component ({
    selector: 'product-form',
    template: `
           .
           .
           .
    `,
    directives: [FORM_DIRECTIVES]
})
export class ProductFormComponent {
```

**FORM_DIRECTIVES** is an Angular 2 shorthand of injecting following directives into your template:

* ngControl (mentioned Control)
* ngControlGroup (mentioned ControlGroup)
* ngForm
* ngModel

### Defining the form template in Angular 2 ###

Let's add into my example of product list ability of adding the Products into product list. 
To do that let's create **ProductFormComponent**:

```
import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {ProductType} from './product';
import {FORM_DIRECTIVES} from 'angular2/common';

@Component ({
    selector: 'product-form',
    template: `
           <h1>Enter new product details</h1>
           <form *ngIf="active" #f="ngForm" (ngSubmit) = 'onSubmit(f.value)'>
                <div>Enter name: <input type="text" ngControl="name"/></div>
                <div>Enter price: <input type="text" ngControl="price"/></div>
                <div>Enter image: <input type="text" ngControl="image"/></div> 
                <div><button type="submit">CREATE</button></div>
           </form> 
    `,
    directives: [FORM_DIRECTIVES]
})
export class ProductFormComponent {
    @Input() productList: ProductType[];
    @Output() productListChange = new EventEmitter<ProductType[]>();

    active: boolean = true;

    onSubmit(submitedValue: ProductType) {
        this.productList.push(submitedValue);
        this.productListChange.emit(this.productList);
        alert('Product has been added!');
        this.resetForm();
    }

    resetForm() : void {
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }
}
```
Things to notice:
-----------------
We wrote the form tag in the following way:

```
<form #f="ngForm" (ngSubmit)="onSubmit(f.value)"
```

By writing **#f="ngForm"** we're creating **local template variable** #f for accessing the **form's ControlGroup** exposed by ngForm directive via **ngForm name**.
Btw something non-obvious happened here. Since ngForm has selector 'form' then this directive was attached in the moment we injected FORM_DIRECTIVES
constant **into our form tag**. NgForm directive gives us two things we can work with:

* ControlGroup of the form, named also ngForm. 
* (ngSubmit) - submit hook.

In our template by code "...**ngControl="<name of control>**" we also added three Controls into ControlGroup exposed by ngForm directive. Name, price and image URL. 

```
<form *ngIf="active" #f="ngForm" (ngSubmit) = 'onSubmit(f.value)'>
                <div>Enter name: <input type="text" ngControl="name"/></div>
                <div>Enter price: <input type="text" ngControl="price"/></div>
                <div>Enter image: <input type="text" ngControl="image"/></div> 
                <div><button type="submit">CREATE</button></div>
           </form> 
```
**In Angular 2 this means that form's ControlGroup will containt submited values as key[name of Control]/value[entered value in input] pairs!** We're getting
this value in (ngSubmit) = 'onSubmit(**f.value**)' where "f" is an variable for accessing the form's ControlGroup, remember?...Now sweet thing is that **if our
controls are named as field of our ProductType object then f.value will be automatically converted by TypeScript to ProductType** so we can write onSubmit like this:


```
.
.
    onSubmit(submitedValue: ProductType) {
        this.productList.push(submitedValue);
        this.productListChange.emit(this.productList);
        alert('Product has been added!');
        this.resetForm();
    }
```
Isn't this nice and effective? Now you should recognize **that submited Product will be emitted out of our ProductFormComponent via two-way databinding**.
So we can see the submitted result immediatelly in our Product list. Let's add our product-form component into our root component.


```
import {Component, OnInit, ViewChild} from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {ProductDetailComponent} from './product.detail.component';
import {ProductFormComponent} from './product-form.component';

@Component({
    selector: 'my-app',
    template: `<h1>Intro to Angular 2 forms</h1>
            <div>
                <product-form [(productList)] = "_products"></product-form>
            </div>
            <hr>
            <ul>
                <li *ngFor="#product of _products">
                    <product-detail [product]="product"></product-detail>
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [ProductDetailComponent, ProductFormComponent]
})
export class AppComponent implements OnInit {
    private _products: ProductType[];

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }
}
```
Notice how we achieved that submited Product will be accessible to "_products" variable. "**<product-form [(productList)] = "_products"></product-form>**" 
Every "productListChange" will save new ProductType into "_product" variable and make it visibile there....Try that out!

### Resetting the form ###

In current version of Angular 2 you can reset the form to pristine state by re-creating the form tag:

```
<form *ngIf=active
```
now by calling reset method after adding the new ProductType, form tag will be recreated in the blink of an eye:

```
this.active = false;
setTimeout(() => this.active = true, 0);
```

# Running the demo #

* git clone <this repo>
* npm install
* npm start
* visit http://localhost:3000

Hope you found this usefull. Take this just as introduction to forms.
We didn't dive into validations and states of a form, this will come next.

see you

Tomas